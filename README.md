# PyLogFileParser v2 #

This is a revised version of the original Java & Python log file parser I created. Rather than as a run once standalone script this version runs in a loop checking a target folder for new files. The earlier implementation also pre-opens the files before processing. This one opens and closes them only on request due to the large number of files involved in this scenario.