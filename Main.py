import sys, os, glob, threading, logging, logging.config, time, ntpath
from parsers.Log4jFileParser import Log4jFileParser
from datetime import datetime,timedelta

# logging settings
logging.config.fileConfig('logging.conf')

# create logger
logger = logging.getLogger(__name__)

# App behavior settings
logFolder = 'L:\\Job??[2-9]\\Run??????\\Logs\\??_???????_Log.txt'

# Listen to this folder
liveFolder = 'M:\\??????*_*-?_????????.req'

batchThreshold = 25 # higher number means less threads created.
totalThreadsTime = timedelta(0)
threadsTime = []

class ProcessThread(threading.Thread):
	policyNumber = ''
	quoteNumber = ''
	folderDate = ''
	workFile = ''

	def __init__(self, policyNumber, quoteNumber, folderDate, workFile):
		threading.Thread.__init__(self)
		#self.name = serverName + str(threadNo)
		self.policyNumber = policyNumber
		self.quoteNumber = quoteNumber
		self.folderDate = folderDate
		self.workFile = workFile

	def run(self):
		global threadsTime
		global totalThreadsTime
		tTime = datetime.now()
		dataList = []
		logger.info('Spawning thread for policy %s, quote %s on %s', self.policyNumber, self.quoteNumber, self.folderDate)

		filesFound = getLogs(logFolder, self.folderDate)
		logger.info("Files found: %s", str(len(filesFound)))
		resultMessage = processLogs(filesFound, self.policyNumber, self.quoteNumber)
		resultLog = open(self.workFile, 'w+')
		resultLog.write(resultMessage)
		resultLog.close()

		resultFile = self.workFile.rstrip('.wrk') + '.res'
		os.rename(self.workFile, resultFile)
		logger.info('Result file generated %s', resultFile)
		
		logger.info('Exiting %s', self.name)
		endTime = datetime.now() - tTime
		logger.info("{} time: {}".format(self.name, endTime))
		threadsTime.append(endTime)
		totalThreadsTime = totalThreadsTime + endTime
	
 # Get the logs in a specified folder with the declared date range and pattern.
 # @param logFolder - The folder to to look 
 # @param folderDate - end of file name search range.
 # @return returns true if the process succeeded successfully.
def getLogs(logFolder, folderDate = time.strftime('%m/%d/%Y')):
	"""Get the logs in a specified folder with the declared date range and pattern."""
	logger.info('Getting Files')
	files = []

	for filename in glob.iglob(logFolder):
		"""Assume only current date is searched."""
		if folderDate == time.strftime('%m/%d/%Y', time.gmtime(os.path.getmtime(filename))) or folderDate == 'all':
			files.append(filename)
	return files

# Process a file by extracting the lines we need.
def processLogs(filesFound, policyNumber, quoteNumber):
	parser = Log4jFileParser(filesFound, policyNumber, quoteNumber)
	parser.processFiles()
	return parser.resultMessage

def getPolicyRequests():
	fileDetails = []
	for filename in glob.iglob(liveFolder):
		basefile = ntpath.basename(filename)

		# Change file to working status
		workFile = filename.rstrip('.req') + '.wrk'
		os.remove(filename)

		# Create blank work file
		fp = open(workFile, 'w+')
		fp.close()

		fileDetail = basefile.rstrip('.req').split('_')
		fileDetail.append(workFile)
		fileDetails.append(fileDetail)
		return fileDetails

def listenerMode():
	"""Listen to active folder for policy requests"""
	while 1:
		time.sleep(10)
		fileDetails = getPolicyRequests()
		if fileDetails is not None:
			logger.info("Policy files were added.")
			# Spawn a new thread for each policy
			for fileDetail in fileDetails:
				logger.info(fileDetail)
				policyNumber = fileDetail[0]
				quoteNumber = fileDetail[1]
				folderDate = datetime.strptime(fileDetail[2], '%m%d%Y').strftime('%m/%d/%Y')
				workFile = fileDetail[3]
				
				thread = ProcessThread(policyNumber, quoteNumber, folderDate, workFile)
				thread.start()
				logger.info('Active sub threads %s', threading.active_count() - 1)
			
def main(args):
	if (len(args) < 3):
		if (args[1] == 'listenerMode'):
			logger.info("Script running in listener mode")
			listenerMode()
		else:
			print('Usage: run <policy number> <mm/dd/yyy> (optional defaults to current date)')
	else:
		policyNumber = args[1]
		quoteNumber = args[2]
		if (len(args) > 3 ):
			folderDate = args[3]
			filesFound = getLogs(logFolder, folderDate)
		else:
			filesFound = getLogs(logFolder)
		logger.info('Searching policy number: %s', policyNumber)
		
		logger.info("Files found: %s", str(len(filesFound)))
		processLogs(filesFound, policyNumber, quoteNumber)

startTime = datetime.now()
main(sys.argv)
logger.info("Script time: {}".format(datetime.now() - startTime))
logging.shutdown()