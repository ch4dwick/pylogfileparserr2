import re, os, logging, shutil, glob

logger = logging.getLogger()

class Log4jFileParser():
	pdfOutputFolder = 'M:\\'
	policyNumber = ''
	quoteNumber = ''
	resultMessage = ''
	inputFiles = []
	inputFile = None
	logLinePattern = re.compile("[0-9]{4}/[0-9]{2}/[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3} .*")
	"""Regex rules to look for in the log file"""
	logDetailPatterns = {
		'getPolicy' : re.compile("[\s]*[\d]*[\s]*[\d]*[\s]*[\d\w]{7,9}[\s]*[\w\d]*[\s]*[\d]{1,7}-[\d]{1}[\s]*[\d]{4}\/[\d]{2}\/[\d]{2} [\d]{2}:[\d]{2}:[\d]{2}[\s]*[\d]{0,3}[\s]*[\d]{0,3}", re.MULTILINE),
		'getEDSInsd' : re.compile("[\d]{4}\/[\d]{2}\/[\d]{2} [\d]{2}:[\d]{2}:[\d]{2}.[\d]{3} MergeJobOutputEDS.GenerateFilesForEDS...Storing in EDS: [\w]{1}[\d]{9}_Q[\d]{7}_[\d]{1}_INSD\.pdf"),
		'getEDSPrdr' : re.compile("[\d]{4}\/[\d]{2}\/[\d]{2} [\d]{2}:[\d]{2}:[\d]{2}.[\d]{3} MergeJobOutputEDS.GenerateFilesForEDS...Storing in EDS: [\w]{1}[\d]{9}_Q[\d]{7}_[\d]{1}_WPRO\.pdf"),
		'getEDSAdin' : re.compile("[\d]{4}\/[\d]{2}\/[\d]{2} [\d]{2}:[\d]{2}:[\d]{2}.[\d]{3} MergeJobOutputEDS.GenerateFilesForEDS...Storing in EDS: [\w]{1}[\d]{9}_Q[\d]{7}_[\d]{1}_ADIN\.pdf"),
		'getEDSMort' : re.compile("[\d]{4}\/[\d]{2}\/[\d]{2} [\d]{2}:[\d]{2}:[\d]{2}.[\d]{3} MergeJobOutputEDS.GenerateFilesForEDS...Storing in EDS: [\w]{1}[\d]{9}_Q[\d]{7}_[\d]{1}_MORT\.pdf"),
		'getEDSFmor' : re.compile("[\d]{4}\/[\d]{2}\/[\d]{2} [\d]{2}:[\d]{2}:[\d]{2}.[\d]{3} MergeJobOutputEDS.GenerateFilesForEDS...Storing in EDS: [\w]{1}[\d]{9}_Q[\d]{7}_[\d]{1}_FMOR\.pdf"),
		'getPrintedInsd' : re.compile("\[[\d]{4}\][\s]*[\d]{0,3}[\s]*[\d]{0,3}[\s]*[\d]{0,3}[\s]*[\d]{0,3}[\s]*[\d]{0,3}[\s]*[\d]{0,3}[\s]*(False|True)[\s]*(False|True)[\s]*INSD[\s]*[\w\d]*_INSD[0-9]{1,2}_Print\.pdf[\s]*Prd: [0-9]{7}[\s]*Pol: [\d]{9} \(INSD\)", re.MULTILINE),
		'getPrintedPrdr' : re.compile("\[[\d]{4}\][\s]*[\d]{0,3}[\s]*[\d]{0,3}[\s]*[\d]{0,3}[\s]*[\d]{0,3}[\s]*[\d]{0,3}[\s]*[\d]{0,3}[\s]*(False|True)[\s]*(False|True)[\s]*WPRO[\s]*[\w\d]*_WPRO[0-9]{1,2}_Print\.pdf[\s]*Prd: [0-9]{7}[\s]*Pol: [\d]{9} \(WPRO\)", re.MULTILINE),
		'getPrintedAdin' : re.compile("\[[\d]{4}\][\s]*[\d]{0,3}[\s]*[\d]{0,3}[\s]*[\d]{0,3}[\s]*[\d]{0,3}[\s]*[\d]{0,3}[\s]*[\d]{0,3}[\s]*(False|True)[\s]*(False|True)[\s]*ADIN[\s]*[\w\d]*_ADIN[0-9]{1,2}_Print\.pdf[\s]*Prd: [0-9]{7}[\s]*Pol: [\d]{9} \(ADIN\)", re.MULTILINE),
		'getPrintedMort' : re.compile("\[[\d]{4}\][\s]*[\d]{0,3}[\s]*[\d]{0,3}[\s]*[\d]{0,3}[\s]*[\d]{0,3}[\s]*[\d]{0,3}[\s]*[\d]{0,3}[\s]*(False|True)[\s]*(False|True)[\s]*MORT[\s]*[\w\d]*_MORT[0-9]{1,2}_Print\.pdf[\s]*Prd: [0-9]{7}[\s]*Pol: [\d]{9} \(MORT\)", re.MULTILINE),
		'getPrintedFmor' : re.compile("\[[\d]{4}\][\s]*[\d]{0,3}[\s]*[\d]{0,3}[\s]*[\d]{0,3}[\s]*[\d]{0,3}[\s]*[\d]{0,3}[\s]*[\d]{0,3}[\s]*(False|True)[\s]*(False|True)[\s]*FMOR[\s]*[\w\d]*_FMOR[0-9]{1,2}_Print\.pdf[\s]*Prd: [0-9]{7}[\s]*Pol: [\d]{9} \(FMOR\)", re.MULTILINE)
		}

	def __init__(self, inputFiles, policyNumber, quoteNumber):
		self.inputFiles = inputFiles
		self.policyNumber = policyNumber
		self.quoteNumber = quoteNumber
		
	def processFiles(self):
		foundFiles = []
		for f in self.inputFiles:
			fd = open(f,'r')
			logData = self.processFile(fd)
			if logData is not None:
				foundFiles.append(logData)
			fd.close()

		if len(foundFiles) > 0:
			logger.info("Relevant files found: %s", len(foundFiles))
			for log in foundFiles:
				self.findPolicyDetail(log)

	def processFile(self, fd):
		"""Convert the input file to a buffer array."""
		match = ''
		bufferLines = ''
		logData = []
		
		#logger.info('Reading file... %s', self.inputFile.name)
		for line in fd:
			# Catch the first line in the log entry. And make sure it wasn't an entry 
			# previously. Otherwise, this is a new log entry.
			if re.match(self.logLinePattern, line) is not None:
				logData.append(match + bufferLines)
				match = line
				bufferLines = ''

			else:
				# No match means this is a line below a log entry
				bufferLines += line
		else:
			# If there's still more left in the buffere after the loop this must be part of the last entry.
			if bufferLines is not None:
				logData.append(match + bufferLines)
				bufferLines = ''
		#logger.info('File length: %d', os.path.getsize(self.inputFile.name))
		
		# Check if the log data contains the policy
		return logData if self.findPolicy(logData, fd.name) else None


		
	def findPolicy(self, logData, fileName):
		logsFound = False
		for i,entry in enumerate(logData):
			if len(re.findall(self.logDetailPatterns['getPolicy'], entry)) > 0 \
			and self.policyNumber in entry \
			and self.quoteNumber in entry:
				logger.info('Policy found in ' + fileName)
				logger.info(entry)
				logsFound = True
				self.resultMessage += 'Policy ' + self.policyNumber + ' '  + self.quoteNumber + ' found.\n'
				self.resultMessage += 'Policy found in ' + fileName + '.\n'

				# Copy generated pdf if available
				pdfFolder = os.path.dirname(fileName) + '\\..\\MergedPDF\\*.pdf'
				for pdfFile in glob.glob(pdfFolder):
				    shutil.copy(pdfFile, self.pdfOutputFolder + self.policyNumber + '_' + self.quoteNumber + '.pdf')
				break
		return logsFound
			

	def findPolicyDetail(self, logData):
		"""Find the policy details pertaining to Print insured/producer and Copy to insured/producer"""
		policyFileFormat = 'P' + self.policyNumber + '_Q' +  self.quoteNumber.replace('-','_')
		for i,entry in enumerate(logData):
			if len(re.findall(self.logDetailPatterns['getPrintedInsd'], entry)) > 0 \
			and self.policyNumber in entry:
				logger.info('%s %s INSD printed', self.policyNumber, self.quoteNumber)
				logger.debug(entry)
				self.resultMessage += self.policyNumber + ' ' + self.quoteNumber + ' INSD printed.\n'
			if len(re.findall(self.logDetailPatterns['getPrintedPrdr'], entry)) > 0 \
			and self.policyNumber in entry:
				logger.info('%s %s WPRO printed', self.policyNumber, self.quoteNumber)
				logger.debug(entry)
				self.resultMessage +=self.policyNumber + ' ' + self.quoteNumber + ' WPRO printed.\n'
			if len(re.findall(self.logDetailPatterns['getPrintedAdin'], entry)) > 0 \
			and self.policyNumber in entry:
				logger.info('%s %s ADIN printed', self.policyNumber, self.quoteNumber)
				logger.debug(entry)
				self.resultMessage +=self.policyNumber + ' ' + self.quoteNumber + ' ADIN printed.\n'
			if len(re.findall(self.logDetailPatterns['getPrintedMort'], entry)) > 0 \
			and self.policyNumber in entry:
				logger.info('%s %s MORT printed', self.policyNumber, self.quoteNumber)
				logger.debug(entry)
				self.resultMessage +=self.policyNumber + ' ' + self.quoteNumber + ' MORT printed.\n'
			if len(re.findall(self.logDetailPatterns['getPrintedFmor'], entry)) > 0 \
			and self.policyNumber in entry:
				logger.info('%s %s FMOR printed', self.policyNumber, self.quoteNumber)
				logger.debug(entry)
				self.resultMessage +=self.policyNumber + ' ' + self.quoteNumber + ' FMOR printed.\n'
			
			if re.match(self.logDetailPatterns['getEDSInsd'], entry) is not None \
			and policyFileFormat in entry:
				logger.info('%s %s INSD copy', self.policyNumber, self.quoteNumber)
				logger.debug(entry)
				self.resultMessage += self.policyNumber + ' ' + self.quoteNumber + ' INSD copy.\n'
			if re.match(self.logDetailPatterns['getEDSPrdr'], entry) is not None \
			and policyFileFormat in entry:
				logger.info('%s %s WPRO copy', self.policyNumber, self.quoteNumber)
				logger.debug(entry)
				self.resultMessage += self.policyNumber + ' ' + self.quoteNumber + ' WPRO copy.\n'
			if re.match(self.logDetailPatterns['getEDSAdin'], entry) is not None \
			and policyFileFormat in entry:
				logger.info('%s %s ADIN copy', self.policyNumber, self.quoteNumber)
				logger.debug(entry)
				self.resultMessage += self.policyNumber + ' ' + self.quoteNumber + ' ADIN copy.\n'
			if re.match(self.logDetailPatterns['getEDSMort'], entry) is not None \
			and policyFileFormat in entry:
				logger.info('%s %s MORT copy', self.policyNumber, self.quoteNumber)
				logger.debug(entry)
				self.resultMessage += self.policyNumber + ' ' + self.quoteNumber + ' MORT copy.\n'
			if re.match(self.logDetailPatterns['getEDSFmor'], entry) is not None \
			and policyFileFormat in entry:
				logger.info('%s %s FMOR copy', self.policyNumber, self.quoteNumber)
				logger.debug(entry)
				self.resultMessage += self.policyNumber + ' ' + self.quoteNumber + ' FMOR copy.\n'
